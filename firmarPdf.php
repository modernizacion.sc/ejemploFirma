<?php
require('fpdf184/fpdf.php');

//Datos capturados del formulario index

$cuil = $_POST['cuil'];
$tipo_documento = $_POST['documento'];

$campo1 = $_POST['inputCampo1'];
$campo2 = $_POST['inputCampo2'];
$campo3 = $_POST['inputCampo3'];

//Creamos un objeto fpdf
$pdf=new FPDF();
//Agregamos una pagina
$pdf->AddPage();

//Agregamos un titulo
$pdf->SetFont('Arial', 'B', 24);
$pdf->Cell(0, 10, $campo1, 0, 1);
$pdf->Ln();

//Agregamos el resto del texto
$pdf->SetFont('Arial', '', 12);
$pdf->MultiCell(0, 7, utf8_decode($campo2), 0, 1);
$pdf->Ln();
$pdf->MultiCell(0, 7, utf8_decode($campo3), 0, 1);
$pdf->Ln();

//Retornamos el documento como una cadena (con la opcion S)
$cadena = $pdf->Output('S');


$url = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

$link = $_SERVER['PHP_SELF'];
$link_array = explode('/',$link);

$url_redirect=str_replace(end($link_array),"",$url);

//Login
$curl = curl_init();
$idTransaccion = uniqid();
curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://firmadigital.santacruz.gob.ar/firmar/v1/login',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => '{
	"usuario": "Usuario",
  	"password": "Password"
}', CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'),
));

$response = curl_exec($curl);

curl_close($curl);

$array = json_decode($response, true);

$token = $array['access_token'];

$nombreArchivo = uniqid();

//Codificamos en base 64 el string del pdf generado anteriormente
$documento = base64_encode($cadena);

// Se debe modificar con el dominio donde se a levantado el ejemplo

$url_redirect = "https://modernizacion.santacruz.gob.ar/ejemploFirma/descargarPdf.php?idTransaccion=".$idTransaccion;

//Envio de informacion a Firmar-API

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://firmadigital.santacruz.gob.ar/firmar/firmador',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => '{
    "cuil": "' . $cuil . '",
    "documento": "' . $documento . '",
    "metadata": {
      "sistemaOrigen" : "origen"
    },
    "type": "' . $tipo_documento . '",
    "urlRedirect": "' . $url_redirect . '"
    }',
    CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer ' . $token . '',
        'Content-Type: application/json',
    ),
));
$response = curl_exec($curl);
//Capturar idTransaccion
$json= json_decode($response);
$arr = array('idTransaccion'=> $idTransaccion, 'idArchivo'=> $json->IdArchivo);


//Creamos el JSON
$json_string = json_encode($arr);
$file = 'Transacciones/transaccion'.$idTransaccion.'.json';
file_put_contents($file, $json_string);

header("Location:" . $json->location);

curl_close($curl);
