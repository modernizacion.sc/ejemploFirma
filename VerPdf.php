<!DOCTYPE html>

<html lang="es">

    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </head>

    <body>
        <div class="container justify-content-center ">
            <div class="card">
            <?php 
            $File="Pdfs_Firmados/".$_GET["idArchivo"].".pdf";
            if(file_exists($File)){ ?>
                    <div class="card-body">
                        <h1 class="text-center">El Documento fue firmado con exito</h1>
                        </br>
                    	<div class="d-grid gap-2 d-md-flex justify-content-center">    

							<a class="btn btn-info btn-lg btn-block" href="<?php echo($File) ?>" >
								Descargar PDF Firmado
							</a>

							<a class="btn btn-primary" href="/">
								Volver a la página principal
							</a>
						</div>

                    </div>
                <?php } else { ?>
                                   
                    <div class="container">

                        <div class="card">

                            <div class="card-header text-center">
                                <h1>Ejemplo - Integración PFDR</h1>
                            </div>

                            <div class="card-body text-center alert alert-warning" role="alert">
                                El documento no se pudo firmar
                            </div>

                            <div class="card-footer"> 
                                                
                                <a class="btn btn-primary" href="{{ route('home') }}">
                                    Volver a la página principal
                                </a>

                            </div>
                        </div>

                    </div>
                <?php } ?>
            </div>

        </div>
		
	</body>
    
</html>
